package com.example.cwk2mobapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.AVAILABILITY;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class Main3Activity extends AppCompatActivity {

    private ListView ingredientsShow;
    private TextView ingredientsDatabase;
    private Button addKitchen;

    private ingredientsData activity;
    private AvailabilityData activityAvailabilty;
    private static String[] FROM = {_ID, NAMEOFPRODUCT, WEIGHT, PRICE, DESCRIPTION};
    private static String ORDER_BY = NAMEOFPRODUCT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = new ingredientsData(Main3Activity.this);
        activityAvailabilty = new AvailabilityData(Main3Activity.this);

        setContentView(R.layout.activity_main3);

        ingredientsShow = (ListView) findViewById(R.id.ingredients2);
        ingredientsDatabase = (TextView) findViewById(R.id.ingredientsDatabase1);

        final Cursor cursor = getIngredients();
        showIngredients(cursor);


        /**
         *
         */
        addKitchen = (Button) findViewById(R.id.addToKitchen);
        addKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray result = ingredientsShow.getCheckedItemPositions();
                for(int idx = 0; idx <= result.size(); idx++) {
                    int selected = result.keyAt(idx);
                    if(result.valueAt(idx)) {
                        System.out.println("result: " + ingredientsShow.getItemAtPosition(selected));
                        String[] splitRecord = ingredientsShow.getItemAtPosition(selected).toString().split(": ");
                        // add to kitchen
                        activityAvailabilty.saveData(splitRecord[1], Double.parseDouble(splitRecord[2]), Double.parseDouble(splitRecord[3]),
                                splitRecord[4], Double.parseDouble("1"));

//                        activityAvailabilty.onUpdate(0, Integer.parseInt(splitRecord[0]));
                        /**
                         * Question 4
                         * Show only available products
                         * if(availability == 1) {
                         * show
                         * }
                         *
                         * update if user deselects
                         * update table set availability = 0 where _ID = splitRecord[0]
                         * activityAvailabilty.onUgrade()
                         */

//                    }else{
//                        String[] splitRecord = ingredientsShow.getItemAtPosition(selected).toString().split(": ");
//                        activityAvailabilty.saveData(splitRecord[1], Double.parseDouble(splitRecord[2]), Double.parseDouble(splitRecord[3]),
//                                splitRecord[4], Double.parseDouble("0"));
//                    }

                    }
                }
                System.out.println("String   " + ingredientsShow.getCheckedItemPositions().toString());
            }
        });
    }

    public Cursor getIngredients() {
        /* Perform a managed query . The Activity will
        handle closing and re - querying the cursor
        when needed . */
        SQLiteDatabase db = activity.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME_INGREDIENTS, FROM, null,
                null, null, null,
                ORDER_BY);

//        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null,null);
        return cursor;
    }

    public void showIngredients(Cursor cursor) {
// Stuff them all into a big string

        String[] elements = new String[cursor.getCount()];
        int index = 0;
        while (cursor.moveToNext()) {
            StringBuilder builder = new StringBuilder();
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            double weights = cursor.getDouble(2);
            double price = cursor.getDouble(3);
            String description = cursor.getString(4);
            builder.append(id).append(": ");
            builder.append(name).append(": ");
            builder.append(weights).append(": ");
            builder.append(price).append(": ");
            builder.append(description).append (": \n" );
//            LinearLayout layout = (LinearLayout)findViewById(R.id.l_layout);
//            CheckBox checkBox = new CheckBox(this);
//            layout.addView(checkBox);
            elements[index++] = builder.toString();

        }
        cursor.close();
        // Display on the screen
        ingredientsDatabase = (TextView) findViewById(R.id.ingredientsDatabase1);

        ingredientsShow = (ListView) findViewById(R.id.ingredients2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (Main3Activity.this,
                        android.R.layout.simple_list_item_multiple_choice,
                        android.R.id.text1, elements );
        ingredientsShow.setAdapter(adapter);


        // ingredientsShow.setText(builder);


        /**
         * for each ingredient in the cursor
         * create new tick + textview
         *
         */
    }



}