package com.example.cwk2mobapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class Main6Activity extends AppCompatActivity {

    private SearchView searchView;
    private ListView listOfIngredients;
    private EditText searchText;
    private Button lookup;
    private ingredientsData iData1;
    private AvailabilityData aData1;
    private static String[] FROM = {_ID, NAMEOFPRODUCT, WEIGHT, PRICE, DESCRIPTION};
    private static String ORDER_BY = NAMEOFPRODUCT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        iData1 = new ingredientsData(Main6Activity.this);
        aData1 = new AvailabilityData(Main6Activity.this);
        lookup = (Button) findViewById(R.id.lookup);
        lookup.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                searchText = (EditText) findViewById(R.id.searchText);
                String userSearch = searchText.getText().toString();
                System.out.println("UserSearch =            " + userSearch);

                System.out.println("HELLOOOOOOOOOOOOOOO        " + iData1.findSimilarProduct(userSearch).toString());
                Cursor cursor = iData1.findSimilarProduct(userSearch);
                Cursor cursorOne = iData1.findSimilarProductOne(userSearch);
                showIngredients(cursor);
                showIngredients(cursorOne);




            }
        });


    }

    public void showIngredients(Cursor cursor) {
    // Stuff them all into a big string

        String[] elements = new String[cursor.getCount()];
        int index = 0;
        while (cursor.moveToNext()) {
            StringBuilder builder = new StringBuilder();
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            double weights = cursor.getDouble(2);
            double price = cursor.getDouble(3);
            String description = cursor.getString(4);
            builder.append(id).append(": ");
            builder.append(name).append(": ");
            builder.append(weights).append(": ");
            builder.append(price).append(": ");
            builder.append(description).append (": \n" );
//            LinearLayout layout = (LinearLayout)findViewById(R.id.l_layout);
//            CheckBox checkBox = new CheckBox(this);
//            layout.addView(checkBox);
            elements[index++] = builder.toString();

        }
        cursor.close();
//        // Display on the screen
        listOfIngredients = (ListView) findViewById(R.id.listOfIngredients);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (Main6Activity.this,
                        android.R.layout.simple_list_item_multiple_choice,
                        android.R.id.text1, elements );
        listOfIngredients.setAdapter(adapter);


    }
}