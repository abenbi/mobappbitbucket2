package com.example.cwk2mobapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.NavigableMap;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.AVAILABILITY;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_AVAILABILITY;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class Main4Activity extends AppCompatActivity {
    private AvailabilityData available;
    private static final String TAG = "AvailabileProd";
    private ListView availableShow;
    private Button saveBtn;
    private static String[] FROM = {_ID, NAMEOFPRODUCT, WEIGHT, PRICE, DESCRIPTION, AVAILABILITY};
    private static String ORDER_BY = NAMEOFPRODUCT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        available = new AvailabilityData(Main4Activity.this);

        setContentView(R.layout.activity_main4);
        availableShow = (ListView)findViewById(R.id.availableProductsList);
        final Cursor cursor = getAvailableIngredients();
        showAvailability(cursor);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                availableShow.getCount()
//                availableShow.setSelected(true);
                SparseBooleanArray checkedPositions = availableShow.getCheckedItemPositions();

                ArrayList<Integer> selectedKeys = new ArrayList<>();
                for(int i = 0; i < checkedPositions.size(); i++) {
                    selectedKeys.add(checkedPositions.keyAt(i));
                }

                for(int i=0; i< availableShow.getCount(); i++) {
                    if(!selectedKeys.contains(i)) {
                        System.out.println("===========================");
                        System.out.println(availableShow.getItemAtPosition(i));
                        String[] split = availableShow.getItemAtPosition(i).toString().split(": ");
                        available.onUpdate(0, Integer.parseInt(split[0]));

                    }
                }
//                saveData(NAMEOFPRODUCT, Double.parseDouble(PRICE), Double.parseDouble(WEIGHT), DESCRIPTION, Double.parseDouble(AVAILABILITY));
            }
        });
    }

    public String findAvailableProduct(String name) {
        SQLiteDatabase db = available.getReadableDatabase();
        String where = Constants.NAMEOFPRODUCT + " = '" + name + "'";
        // SELECT * from availability WHERE Name like '%LETTUCE%'
        Cursor cursor = db.query(TABLE_NAME_INGREDIENTS, FROM, where,null, null, null,null);
        if(cursor != null) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                cursor.close();
                return id;
            }
        }
        cursor.close();
        return null;
    }

    public Boolean saveData(String name, Double weight, Double price, String description, Double availability) {
        SQLiteDatabase db = available.getWritableDatabase();
//        onCreate(db);
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAMEOFPRODUCT, name);
        contentValues.put(WEIGHT, weight);
        contentValues.put(PRICE, price);
        contentValues.put(DESCRIPTION, description);
        contentValues.put(AVAILABILITY, availability);
        Log.d(TAG, "ADD DATA: ADDING " + name + "to " + TABLE_NAME_AVAILABILITY);
        String id = findAvailableProduct(name);
        if(id == null) {
            long result = db.insert(TABLE_NAME_AVAILABILITY, null, contentValues);
            if (result == -1) {
                System.out.println(" false");
                return false;
            } else {
                System.out.println(" true");
                return true;
            }
        } else {
            available.onUpdate(1, Integer.parseInt(id));
            return true;
        }
    }
    public void showAvailability(Cursor cursor) {

//        String[] elements1 = new String[cursor.getCount()];
        ArrayList<String> elements1 = new ArrayList<>();
        int index = 0;
        while (cursor.moveToNext()) {
            StringBuilder builder = new StringBuilder();
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            double weights = cursor.getDouble(2);
            double price = cursor.getDouble(3);
            String description = cursor.getString(4);
            double availability = cursor.getDouble(5);
            builder.append(id).append(": ");
            builder.append(name).append(": ");
            builder.append(weights).append(": ");
            builder.append(price).append(": ");
            builder.append(description).append(": ");
            builder.append(availability).append(": ");
            if( availability == 1){
                elements1.add(builder.toString());
            } else {
//                elements1[index] = "N/A";
            }
            index++;
            System.out.println("builder  " + builder.toString());
        }
        cursor.close();
        availableShow = (ListView)findViewById(R.id.availableProductsList);
        if(elements1.size() < 1) {
            return;
        }

        for ( int i=0; i < availableShow.getChildCount(); i++) {
            availableShow.setItemChecked(i, true);
        }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (Main4Activity.this,
                            android.R.layout.simple_list_item_multiple_choice,
                            android.R.id.text1, elements1);
            availableShow.setAdapter(adapter);

//                        availableShow.setSelection();

//                        availableShow.setAdapter(adapter);

    }

    public Cursor getAvailableIngredients() {
        /* Perform a managed query . The Activity will
        handle closing and re - querying the cursor
        when needed . */
        SQLiteDatabase db = available.getReadableDatabase();


        Cursor cursor = db.query(TABLE_NAME_AVAILABILITY, FROM, null,
                null, null, null,
                ORDER_BY);

//        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null,null);
        return cursor;
    }


    }
