package com.example.cwk2mobapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditProducts extends AppCompatActivity {

    private TextView idTextView;
    private EditText NOPEdit;
    private EditText weightEdit;
    private EditText priceEdit;
    private EditText descriptionEdit;
    private Button update;
    private TextView updateReview;
    private ingredientsData dataIngredient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_products2);

        dataIngredient = new ingredientsData(EditProducts.this);
        updateReview = (TextView) findViewById(R.id.updateReview);
        int passedArg0 = getIntent().getExtras().getInt("arg0");
        final String passedArg = getIntent().getExtras().getString("arg");
        String passedArg1 = getIntent().getExtras().getString("arg1");
        String passedArg2 = getIntent().getExtras().getString("arg2");
        String passedArg3 = getIntent().getExtras().getString("arg3");

        idTextView = (TextView)findViewById(R.id.idTextView);
//        idTextView.setText(passedArg0);
        System.out.println("HIIIIIIIIIIIIIIIIIII          " + passedArg0);
        NOPEdit = (EditText)findViewById(R.id.NOPEdit);
        NOPEdit.setText(passedArg);
        System.out.println("HIIIIIIIIIIIIIIIIIIIIIIIII       " + NOPEdit.getText().toString());
        weightEdit = (EditText) findViewById(R.id.weightEdit);
        weightEdit.setText(passedArg1);
        System.out.println("HIIIIIIIIIIIIIIII       " +Double.parseDouble(weightEdit.getText().toString()));
        priceEdit = (EditText) findViewById(R.id.priceEdit);
        priceEdit.setText(passedArg2);
        Double.parseDouble(priceEdit.getText().toString());
        descriptionEdit = (EditText)findViewById(R.id.descriptionEdit);
        descriptionEdit.setText(passedArg3);
        System.out.println("HIIIIIIIIIIIIIIIIIII          " + descriptionEdit.getText().toString());
        update = (Button) findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataIngredient.onUpdateIngredients(Integer.parseInt(idTextView.getText().toString()),NOPEdit.getText().toString(), descriptionEdit.getText().toString(), Double.parseDouble(priceEdit.getText().toString()), Double.parseDouble(weightEdit.getText().toString()));
                updateReview.setText("The item you selected in the previous page, has been updated...");
            }
        });


    }
}
