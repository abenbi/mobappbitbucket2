package com.example.cwk2mobapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.AVAILABILITY;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_AVAILABILITY;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class AvailabilityData extends SQLiteOpenHelper {

    private static final String TAG = "Availability";

    private static final String DATABASE_NAME = "availability.db";
    private static final int DATABASE_VERSION = 1;

    public AvailabilityData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = " CREATE TABLE IF NOT EXISTS " + TABLE_NAME_AVAILABILITY + "("
                + _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAMEOFPRODUCT + " TEXT NOT NULL,"
                + WEIGHT + " DOUBLE, "
                + PRICE + " DOUBLE, "
                + DESCRIPTION + " TEXT NOT NULL,"
                + AVAILABILITY + "DOUBLE);";
        db.execSQL(createTable);
        System.out.println("CREATE THIS TABLE    "+ createTable);
        //db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_AVAILABILITY);
        onCreate(db);

    }

    public void onUpdate(int newAvailability, int idIn) {
        //update table_name set availability = 0 where _ID = 1
        SQLiteDatabase db = this.getWritableDatabase();
        String query = " UPDATE " + TABLE_NAME_AVAILABILITY + " set " + AVAILABILITY + " = " + newAvailability + " where _ID = " + idIn;
        // set name = '', descirtion = '', weight =

        db.execSQL(query);
    }

    public String findAvailableProduct(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] fieldList = {_ID};
        String where = Constants.NAMEOFPRODUCT + " = '" + name + "'";
        // SELECT * from availability WHERE Name like '%LETTUCE%'
        Cursor cursor = db.query(TABLE_NAME_AVAILABILITY, fieldList, where,null, null, null,null);
        if(cursor != null) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                cursor.close();
                return id;
            }
        }
        cursor.close();
        //db.close();
        return null;
    }


    public Boolean saveData(String name, Double weight, Double price, String description, Double availability) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAMEOFPRODUCT, name);
        contentValues.put(WEIGHT, weight);
        contentValues.put(PRICE, price);
        contentValues.put(DESCRIPTION, description);
        contentValues.put(AVAILABILITY, availability);
        Log.d(TAG, "ADD DATA: ADDING " + name + "to " + TABLE_NAME_AVAILABILITY);
        String id = findAvailableProduct(name);
//        db.close();
        if(id == null) {
            long result = db.insert(TABLE_NAME_AVAILABILITY, null, contentValues);
            if (result == -1) {
                System.out.println(" false");
                return false;
            } else {
                System.out.println(" true");
                return true;
            }
        } else {
            onUpdate(1, Integer.parseInt(id));
            return true;
        }
    }
}