package com.example.cwk2mobapp;
import android.provider.BaseColumns;

public interface Constants extends BaseColumns {

    public static final String TABLE_NAME_INGREDIENTS = " ingredients " ;
    public static final String TABLE_NAME_AVAILABILITY = " availability " ;
    public static final String NAMEOFPRODUCT = " nameofproduct " ;
    public static final String WEIGHT = " weight " ;
    public static final String PRICE = " price " ;
    public static final String DESCRIPTION = " description " ;
    public static final String AVAILABILITY = " availability " ;



}
