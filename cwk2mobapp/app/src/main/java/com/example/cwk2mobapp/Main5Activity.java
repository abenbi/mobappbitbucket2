package com.example.cwk2mobapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.AVAILABILITY;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_AVAILABILITY;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class Main5Activity extends AppCompatActivity {

    private static final String TAG = "AllProduct";
    private ListView allShow;
    private TextView textView;
    private Button editButton;
    private AvailabilityData availabilityData;
    private ingredientsData dataI;
    private static String[] FROM = {_ID, NAMEOFPRODUCT, WEIGHT, PRICE, DESCRIPTION};
    private static String ORDER_BY = NAMEOFPRODUCT;
    public static final String ARG_FROM_MAIN0 = "arg0";
    public static final String ARG_FROM_MAIN = "arg";
    public static final String ARG_FROM_MAIN1 = "arg1";
    public static final String ARG_FROM_MAIN2 = "arg2";
    public static final String ARG_FROM_MAIN3 = "arg3";

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        availabilityData = new AvailabilityData(Main5Activity.this);
        dataI = new ingredientsData(Main5Activity.this);
        allShow = (ListView) findViewById(R.id.allShow1);
        editButton = (Button) findViewById(R.id.editButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray edit = allShow.getCheckedItemPositions();
                for(int z = 0; z <= edit.size(); z++) {
                    final int editItem = edit.keyAt(z);
                    if (edit.valueAt(z)) {
                        System.out.println("result: " + allShow.getItemAtPosition(editItem));
                        final String[] splitRecord = allShow.getItemAtPosition(editItem).toString().split(": ");
                            textView = (TextView)findViewById(R.id.textView);
                            final String id = splitRecord[0];
                        final String nameEdit = splitRecord[1];
                        System.out.println(splitRecord[1] +  "----------------------");
                        final String weightEdit = splitRecord[2];
                        System.out.println(splitRecord[2] +  "----------------------");
                        final String priceEdit = splitRecord[3];
                        System.out.println(splitRecord[3] +  "----------------------");
                        final String descriptionEdit = splitRecord[4];
                        System.out.println(splitRecord[4] +  "----------------------");
                        textView.setText("You chose to edit the object you selected, press 'Edit' to proceed..." + splitRecord[1]);
                            editButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Main5Activity.this, EditProducts.class);
                                    intent.putExtra("arg0", id);
                                    intent.putExtra("arg", nameEdit);
                                    intent.putExtra("arg1", weightEdit);
                                    intent.putExtra("arg2", priceEdit);
                                    intent.putExtra("arg3", descriptionEdit);
                                    startActivity(intent);
                                }
                            });



                    }
                }
            }
        });

        allShow = (ListView) findViewById(R.id.allShow1);



                Cursor cursor = getIngredients();
                showIngredients(cursor);
                    }

    public Cursor getIngredients() {
        /* Perform a managed query . The Activity will
        handle closing and re - querying the cursor
        when needed . */
        SQLiteDatabase db = dataI.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME_INGREDIENTS, FROM, null,
                null, null, null,
                ORDER_BY);

//        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null,null);
        return cursor;
    }

    public void showIngredients(Cursor cursor) {
// Stuff them all into a big string

        final String[] elements = new String[cursor.getCount()];
        int index = 0;
        while (cursor.moveToNext()) {
            StringBuilder builder = new StringBuilder();
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            double weights = cursor.getDouble(2);
            double price = cursor.getDouble(3);
            String description = cursor.getString(4);
            builder.append(id).append(": ");
            builder.append(name).append(": ");
            builder.append(weights).append(": ");
            builder.append(price).append(": ");
            builder.append(description).append(": \n");
//            LinearLayout layout = (LinearLayout)findViewById(R.id.l_layout);
//            CheckBox checkBox = new CheckBox(this);
//            layout.addView(checkBox);
            elements[index++] = builder.toString();

        }
        cursor.close();
        // Display on the screen

        allShow = (ListView) findViewById(R.id.allShow1);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (Main5Activity.this,
                        android.R.layout.simple_list_item_single_choice,
                        android.R.id.text1, elements);
        allShow.setAdapter(adapter);

//        allShow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent newIntent = new Intent(Main5Activity.this, EditProducts.class);
//                startActivity(newIntent);
//            }
//        });



//        allShow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });



    }

    }
